#This software checks forms

class Form():

    def __init__(self, sku, description, reg_price, sale_price, **additional_info):
        """Initializes the form itself"""
        self.sku = sku
        self.description = description
        self.reg_price = reg_price
        self.sale_price = sale_price
        self.additional_info = additional_info

    def info(self):
        print("FORM INFORMATION:")
        print("-" * 20)
        print(f"SKU:\t\t{self.sku}")
        print(f"Description:\t{self.description}")
        print(f"Regular Price:\t{format(self.reg_price, '.2f')}")
        print(f"Sale Price:\t{format(self.sale_price, '.2f')}")
        if self.additional_info:
            print('-' * 20)
            print("Additional Information:")
            for key, val in self.additional_info.items():
                print(f"{key}:\t\t{val}")

    def update_form(self, what):
        if what.upper() in "formnumbersku".upper():
            new_sku = input("\nWhat is the new SKU? ")
            self.sku = new_sku
            print(f"\nSKU Updated to: {self.sku}\n")

def getfo():
    whitetail1.info()
        
whitetail1 = Form("WT-8541B", "Whitetail Straight", 428.50, 210.85, name='wilfred', age='19', likes='he likes chili')

whitetail1.info()

whitetail1.update_form('form')

getfo() 
